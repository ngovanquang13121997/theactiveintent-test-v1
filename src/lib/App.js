import right_svg from "./assets/right_svg.js";
import spaeaker_image from "./assets/speaker";

var stringSimilarity = require("string-similarity");

let buttonStyle =
  "style='padding: 0;border: none;background: none;cursor: pointer;'";

let status = 0;

class App {
  activationKey = undefined;

  constructor(intents) {
    this.intents = intents;
    if (window.hasOwnProperty("webkitSpeechRecognition")) {
      this.recognition = new window.webkitSpeechRecognition();
      this.recognition.lang = "en-US";
      this.recognition.interimResults = false;
      this.recognition.continuous = false;
    }

    if (window.hasOwnProperty("webkitSpeechGrammarList")) {
      this.speechRecognitionList = new window.webkitSpeechGrammarList();
      this.speechRecognitionList.addFromString("#JSGF V1.0;", 1);
      this.recognition.grammars = this.speechRecognitionList;
    }

    const script = window.document.getElementById("ActiveIntent");
    if (script) {
      this.activationKey = script.getAttribute("activationKey");
    }
    if (!this.activationKey) return;
    this.usingSDKRequest();
  }

  usingSDKRequest = () => {
    let hostname = window.location.hostname;

    let xmlhttp = new XMLHttpRequest();

    if (this.recognition) {
      this.configureRecognition();
      this.addMicrophoneElement();
    }
    this.addSpeakerElements();
    this.addListener();

    xmlhttp.open(
      "GET",
      `https://theactiveintent.com/api/v1/customer/using?hostname=${hostname}&activationKey=${this.activationKey}`,
      true
    );
    xmlhttp.send();
  };

  findTheIntent = (phrase) => {
    for (let intent of this.intents) {
      if (intent.phrases.includes(phrase)) {
        return intent;
      }
      for (let phrase_source of intent.phrases) {
        if (phrase.startsWith(phrase_source)) {
          return intent;
        }
      }
    }
  };

  configureRecognition = () => {
    let findTheIntent = this.findTheIntent;
    let previousIntent;

    this.recognition.onstart = function () {
      const element = document.getElementsByClassName(
        "the_activeintent_permission"
      )[0];
      element.style.display = "none";
    };

    this.recognition.onaudiostart = function () {
      const element = document.getElementsByClassName(
        "the_activeintent_listening"
      )[0];
      element.style.display = "block";

      const imageElement = document.getElementsByClassName(
        "the_activeintent_image"
      )[0];
      imageElement.style.opacity = "1";

      window.dotsGoingUp = true;
      window.setInterval(function () {
        var wait = document.getElementsByClassName("the_activeintent_int")[0];
        if (window.dotsGoingUp) wait.innerHTML += ".";
        else {
          wait.innerHTML = wait.innerHTML.substring(1, wait.innerHTML.length);
          if (wait.innerHTML === "") window.dotsGoingUp = true;
        }
        if (wait.innerHTML.length > 3) window.dotsGoingUp = false;
      }, 400);
    };

    this.recognition.onresult = function (event) {
      status = 0;
      var lastResult = event.results.length - 1;
      var content = event.results[lastResult][0].transcript.trim();
      let intent = findTheIntent(content);

      let message = "";
      if (intent !== undefined) {
        previousIntent = intent;

        if (intent.name === "ArticleIntent") {
          let elements = document.querySelectorAll("article h1");
          if (elements.length === 0) {
            message = "There are no articles";
          } else {
            message = `There are ${elements.length} articles. Choose any one.\r\n`;
            elements.forEach(function (element, i) {
              message += `${i + 1} ${element.textContent.trim()}\r\n`;
            });
          }
        } else {
          let element = document.querySelector(
            "[og-intent='" + intent.name + "']"
          );
          if (element !== null && element !== undefined) {
            message = element.textContent.trim();
          }
        }
      } else if (
        intent === undefined &&
        previousIntent !== undefined &&
        previousIntent.name === "ArticleIntent"
      ) {
        // var similarity = stringSimilarity.compareTwoStrings("healed", "sealed");
        // console.log(similarity);

        let elements = document.querySelectorAll("article h1");
        let list_of_articles = [];
        elements.forEach((element) => {
          list_of_articles.push(element.textContent.trim());
        });
        var matches = stringSimilarity.findBestMatch(content, list_of_articles);
        if (matches.bestMatchIndex !== 0) {
          message = document
            .querySelectorAll("article p")
            [matches.bestMatchIndex].textContent.trim();
        } else {
          message = `No Articles found with ${content}`;
        }
      } else {
        message = "We haven't set it up";
      }

      if (
        intent !== undefined &&
        intent.type !== undefined &&
        intent.type === "action"
      ) {
        for (let phrase of intent.phrases) {
          content = content.replace(phrase, "");
        }
        let elements = document.querySelectorAll(
          "[og-intent='" + intent.name + "']"
        );
        for (let element of elements) {
          if (
            content.trim().toLowerCase() ===
            element.textContent.trim().toLowerCase()
          ) {
            element.click();
          }
        }
      } else {
        speechSynthesis.cancel();
        speechSynthesis.speak(new SpeechSynthesisUtterance(message));
      }
    };

    this.recognition.onaudioend = function () {
      status = 0;
      const elementListening = document.getElementsByClassName(
        "the_activeintent_listening"
      )[0];
      elementListening.style.display = "none";
    };

    this.recognition.onend = function () {
      status = 0;
      const element = document.getElementsByClassName(
        "the_activeintent_permission"
      )[0];
      element.style.display = "none";

      const elementListening = document.getElementsByClassName(
        "the_activeintent_listening"
      )[0];
      elementListening.style.display = "none";

      const imageElement = document.getElementsByClassName(
        "the_activeintent_image"
      )[0];
      imageElement.style.opacity = "0";
    };

    this.recognition.onerror = function (event) {
      status = 0;
    };
  };
  addMicrophoneElement = () => {
    let microphoneElement = document.createElement("div");

    microphoneElement.innerHTML =
      '<div class="the_activeintent" style="position: fixed;bottom: 8%;right: 5%;">' +
      '<div style="display:flex;color: #515151;align-items: center;">' +
      '<div style="position: absolute;right: 100%;">' +
      '<div class="the_activeintent_permission"' +
      'style="background: #EAEEF5;padding: 18px;border-radius: 13px;margin-right: 45px;position: relative;bottom: 40px;width: 198px;outline: none;display:none">' +
      "Enable permissions on <br> the upper left of your <br> browser window" +
      '<div style="position: absolute;right: -30px;bottom: 0;">' +
      right_svg +
      " </div>" +
      "</div>" +
      '<div class="the_activeintent_listening"' +
      'style="background: #EAEEF5;padding: 18px;border-radius: 13px;margin-right: 20px;width: 198px;outline: none;display:none">' +
      "Listening" +
      '<span class="the_activeintent_int" style="padding-left:5px">.</span>' +
      "</div>" +
      "</div>" +
      "<div>" +
      '<button id="microphone" style="padding: 0;border: none;background: none;cursor: pointer;outline: none;display:flex;align-items:center">' +
      '<img src="https://i.ibb.co/SnsCnd6/Group-2.png" style="position:absolute;right: 9px"/>' +
      '<img class="the_activeintent_image" src="https://i.ibb.co/9rW6Wrf/Frame3.gif" width="89px" style="opacity:0"  />' +
      "</button>" +
      "</div>" +
      "</div>" +
      "</div>";

    document.body.appendChild(microphoneElement);
  };

  addSpeakerElements = () => {
    for (let element of document.querySelectorAll("[magic='speakable']")) {
      let miniMicrophoneElement = document.createElement("div");
      miniMicrophoneElement.innerHTML =
        "<div><button class='speaker' " +
        buttonStyle +
        " ><img style='width: 25px;' src='" +
        spaeaker_image +
        "'/> </button></div>";
      element.appendChild(miniMicrophoneElement);
    }
  };

  addListener = () => {
    let recognition = this.recognition;
    let statusPermission = false;
    if (this.recognition) {
      // Check Permission : https://developer.mozilla.org/en-US/docs/Web/API/Permissions/query
      navigator.permissions
        .query({ name: "microphone" })
        .then(function (result) {
          if (result.state == "granted") {
            statusPermission = false;
          } else if (result.state == "prompt") {
            statusPermission = true;
          }
        });
      for (let e of document.querySelectorAll("#microphone")) {
        e.addEventListener("click", function () {
          if (statusPermission && status !== 1) {
            const element = document.getElementsByClassName(
              "the_activeintent_permission"
            )[0];
            element.style.display = "block";
          }

          if (status === 1) {
            status = 0;
            return recognition.stop();
          }

          recognition.start();
          status = 1;
        });
      }
    }

    setInterval(() => {
      let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

      let isChrome = /Chrome/i.test(navigator.userAgent);
      if (!isMobile && isChrome) {
        speechSynthesis.pause();
      }
      speechSynthesis.resume();
    }, 10000); //Fix https://stackoverflow.com/questions/21947730/chrome-speech-synthesis-with-longer-texts

    for (let element of document.querySelectorAll(".speaker")) {
      element.addEventListener("click", function () {
        speechSynthesis.cancel();
        let message =
          this.parentElement.parentElement.parentElement.textContent.trim();

        speechSynthesis.speak(new SpeechSynthesisUtterance(message));
      });
    }
  };
}

export default App;
